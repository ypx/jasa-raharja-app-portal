# How to run this app locally

- Install [Composer](https://getcomposer.org) first
- Clone this onto your local folder, enter the folder and run `composer install` to install dependencies
- Run app with your webserver or using `php -S localhost:$PORT`