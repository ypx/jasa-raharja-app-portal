<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Apps extends CI_Model {

    public function GetApps()
    {
        $apps = '[
            {
              "title": "E-Recruitment",
              "url": "http://erecruitment.jasa.raharja",
              "image": "app-img-1.jpg"
            },
            {
              "title": "E-Lapor",
              "url": "http://elapor.jasa.raharja",
              "image": "app-img-2.jpg"
            },
            {
              "title": "Resi Online",
              "url": "http://resionline.jasa.raharja",
              "image": "app-img-3.jpg"
            },
            {
              "title": "Sistem Informasi Surat",
              "url": "http://sis.jasa.raharja",
              "image": "app-img-4.jpg"
            },
            {
              "title": "E-Form",
              "url": "http://eform.jasa.raharja",
              "image": "app-img-5.jpg"
            },
            {
              "title": "Sistem Monitoring",
              "url": "http://monitoring.jasaraharja.co.id",
              "image": "app-img-6.jpg"
            }
        ]';
        return json_decode($apps);
    }

}

/* End of file M_Apps.php */
/* Location: .//Users/yuri/Repositories/JasaRaharja-Codeigniter/src/models/M_Apps.php */