<?php $this->load->view("_layout/head") ?>

<main role="main">
  <section class="jumbotron text-center">
    <div class="container">
      <img class="logo" src="assets/img/jasa-raharja-logo-vertical.png">
    </div>
  </section>
  <div class="album py-5">
    <div class="container">
      <div class="row">
        <?php foreach($apps as $app): ?>
        <div class="col-md-3">
          <a class="card bg-dark text-white mb-4 box-shadow" href="<?php echo base_url('detail/' . base64_encode($app->url . "|" . $app->title)) ?>">
          <!-- <a class="card bg-dark text-white mb-4 box-shadow" href="<?php echo $app->url ?>"> -->
            <img class="card-img" src="assets/img/<?php echo $app->image ?>">
            <div class="card-img-overlay d-flex align-items-end">
              <p class="card-text">
                <?php echo $app->title ?>
                <small>
                  <i class="fa fa-link"></i> <?php echo $app->url ?>
                </small>
              </p>
            </div>
          </a>
        </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <footer class="text-muted">
    <div class="container">
      <p class="float-right"><a class="text-danger" href="<?php echo base_url('auth/logoutAction') ?>"><i class="fa fa-power-off"></i> Logout</a></p>
      <p>&copy; Jasa Raharja - Portal Aplikasi</p>
    </div>
  </footer>
</main>

<?php $this->load->view("_layout/foot") ?>