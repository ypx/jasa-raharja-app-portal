<?php $this->load->view("_layout/head") ?>

<!-- <nav class="navbar bg-primary sticky-top flex-md-nowrap p-0">
  <a class="navbar-brand col-sm-6 col-md-6 mr-0 text-white" href="#">Jasa Raharja App - <?php echo $title ?></a>
  <ul class="navbar-nav">
    <li>
      <a href="">Kembali</a>
    </li>
    <li class="nav-item text-nowrap">
      <a class="nav-link text-white" href="<?php echo base_url('auth/logoutAction') ?>"><i class="fa fa-power-off"></i> Sign out</a>
    </li>
  </ul>
</nav> -->
<nav class="navbar navbar-expand navbar-dark bg-dark">
      <a class="navbar-brand" href="#">Jasa Raharja App - <?php echo $title ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample02">
        <ul class="navbar-nav mr-auto">
          <!-- <li class="nav-item active">
            <a class="nav-link" href="#"><?php echo $title ?> <span class="sr-only">(current)</span></a>
          </li> -->
        </ul>
        <a href="<?php echo base_url('landing') ?>" class="btn btn-default text-white">
          <i class="fa fa-long-arrow-left"></i> Kembali
        </a>
        <a href="<?php echo base_url('auth/logoutAction') ?>" class="btn btn-danger">
          <i class="fa fa-power-off"></i> Logout
        </a>
      </div>
    </nav>

<div class="container-fluid">
  <div class="row">

    <!-- <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="<?php echo base_url('landing') ?>">
              <i class="fa fa-caret-left"></i> Kembali <span class="sr-only">(current)</span>
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Daftar Aplikasi</span>
          <a class="d-flex align-items-center text-muted" href="#">
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          <?php foreach($apps as $app): ?>
          <li class="nav-item">
            <a class="nav-link <?php if($app->url == $url) echo 'active' ?>" href="<?php echo base_url('detail/' . base64_encode($app->url)) ?>">
              <?php echo $app->title ?>
            </a>
          </li>
        <?php endforeach; ?>
        </ul>
      </div>
    </nav> -->

    <main role="main" class="col-md-12 ml-sm-auto col-lg-12" style="padding-left: 0px; padding-right: 0px">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-stretch pb-2 mb-3" style="height: 600px !important">
        <iframe src="<?php echo $url ?>" frameborder="0"></iframe>
      </div>
    </main>

  </div>
</div>
<?php $this->load->view("_layout/foot") ?>