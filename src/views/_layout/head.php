<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/jasa-raharja-logo-favicon-32x32.png') ?>">

    <title><?php echo $title ?></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="<?php echo base_url('vendor/twbs/bootstrap/dist/css/bootstrap.min.css')?>" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <!-- <link rel="stylesheet" href="<?php echo base_url('vendor/fortawesome/font-awesome/css/font-awesome.min.css') ?>"> -->
    <link href="<?php echo site_url('assets/css/font-face_worksans.css') ?>?<?php echo md5(date('ymdhis')) ?>" rel="stylesheet">
    <link href="<?php echo site_url('assets/css/main.css') ?>?<?php echo md5(date('ymdhis')) ?>" rel="stylesheet">
    <?php $this->load->view("_layout/css") ?>

  </head>

  <body>