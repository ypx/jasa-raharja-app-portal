<?php $this->load->view("_layout/head") ?>


<form class="form-signin" method="POST" action="<?php echo base_url('auth/loginAction') ?>">
  <div class="text-center mb-4">
    <img class="mb-4" src="<?php echo site_url('assets/img/jasa-raharja-logo-symbol.png') ?>" alt="" width="160">
    <!-- <h1 class="h3 mb-3 font-weight-normal">Application Portal</h1> -->
    <!-- <p>Build form controls with floating labels via the <code>:placeholder-shown</code> pseudo-element. <a href="https://caniuse.com/#feat=css-placeholder-shown">Works in latest Chrome, Safari, and Firefox.</a></p> -->
  </div>
  
  <?php if($this->session->flashdata('alert')): ?>
    <div class="alert alert-danger">
      <?php echo $this->session->flashdata('message')?>
    </div>
  <?php endif; ?>

  <div class="form-label-group">
    <input type="text" id="inputEmail" name="username" class="form-control" placeholder="Email address / Username" required autofocus>
    <label for="inputEmail">Email address / Username</label>
  </div>

  <div class="form-label-group">
    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
    <label for="inputPassword">Password</label>
  </div>

  <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
  <p class="mt-5 mb-3 text-muted text-center">&copy; Jasa Raharja Application Portal 2018</p>
</form>

<?php $this->load->view("_layout/foot") ?>