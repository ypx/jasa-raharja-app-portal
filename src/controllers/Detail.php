<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $data['styles'] = [
            // "bs_dashboard.css"
        ];

        $this->load->vars($data);
        $this->load->model('M_Apps', 'Apps');
        if(@$this->session->userdata('username') == null) {
            redirect('auth');
        } 
    }

    public function view($encoded_param)
    {
        $data['title'] = "Jasa Raharja - App";
        $data['apps'] = $this->Apps->GetApps();
        $param = explode("|", base64_decode($encoded_param));
        $data['url'] = $param[0];
        $data['title'] = $param[1];
        // echo "<pre>";
        // print_r($encoded_url);
        // exit;
        $this->load->view('detail/view', $data, FALSE);
    }

}

/* End of file Detail.php */
/* Location: .//Users/yuri/Repositories/JasaRaharja-Codeigniter/src/controllers/Detail.php */