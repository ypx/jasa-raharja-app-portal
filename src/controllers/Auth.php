<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        
        $data['title'] = "Jasa Raharja - Login Portal";

        $data['styles'] = [
            "bs_floating-label.css"
        ];

        $this->load->vars($data);
    }

    public function index()
    {
        $this->session->sess_destroy();
        $this->load->view('auth/view', FALSE);
    }

    public function loginAction()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $response = $this->sendLogin($username, $password);
        $this->session->set_flashdata('alert', true);
        if($response->status == "success") {
            $this->session->set_flashdata('message', 'Login Sukses!');
            $this->session->set_userdata('logged_in', true);
            $this->session->set_userdata('username', $username);
            redirect('landing');
        } else {
            $this->session->set_flashdata('message', $response->message);
            redirect('auth');
        }
    }

    public function logoutAction()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }

    public function sendLogin($username, $password)
    {
        $url = "http://rest.jasaraharja.co.id/index.php/adjr?email={$username}&password={$password}&key=UserJr1961";
        $response = file_get_contents($url);
        return json_decode($response);
    }

}

/* End of file Auth.php */
/* Location: .//Users/yuri/Repositories/JasaRaharja-Codeigniter/src/controllers/Auth.php */