<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $data['title'] = "Jasa Raharja - Login Portal";

        $data['styles'] = [
            "bs_album.css",
            "landing.css"
        ];

        $this->load->vars($data);
        $this->load->model('M_Apps', 'Apps');

        if(@$this->session->userdata('username') == null) {
            redirect('auth');
        }  
    }
    
    public function index()
    {
        $data['apps'] = $this->Apps->GetApps();
        $this->load->view('landing/view', $data, FALSE);
    }

}

/* End of file Landing.php */
/* Location: .//Users/yuri/Repositories/JasaRaharja-Codeigniter/src/controllers/Landing.php */